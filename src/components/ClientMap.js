import React , { Component } from 'react';
import { Map, TileLayer, Marker, Popup } from 'react-leaflet';
import ClientData from '../data.json';
// import ClientDetails from './ClientDetails';

export default class ClientMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      lat: 51.505,
      lng: -0.09,
      zoom: 13,
    };
  }
  render() {
    const position = [this.state.lat, this.state.lng];
    return (
      <Map center={position} zoom={this.state.zoom}>
        <TileLayer
          attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <Marker position={position}>
          <div>
            {ClientData.map((clientDetail, index) =>  {
            return ( <ClientDetails client={clientDetail} key={`client-list-key ${index}`}/>
            )})}
          </div>
        </Marker>
    </Map>
    )
  }
}


