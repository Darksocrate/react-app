import React , { Component } from 'react';
import { styles } from './ClientDetail';


export default class ClientDetails extends Component {

  render() {
    const client = this.props;
    return (
     <div style={styles}>
      <h1>{client.client}</h1>
      <p>Ville {client.ville}</p>
      <p>CP {client.cp}</p>
      <p>Localisation {client.gps}</p>
    </div>
    )
  }
}