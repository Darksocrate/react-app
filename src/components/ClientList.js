import React , { Component } from 'react';
import ClientData from '../data.json';
import { styles } from './ClientDetail';
import ClientDetail from './ClientDetail';

export default class ClientList extends Component {
  
  constructor(props) {
    super(props);
    this.handleCount = this.handleCount.bind(this);
    this.state = {
      count: 20,
      defaultCount: 20,
    };
  }

  handleCount() {
    let count = this.state.defaultCount;
    count = count + this.state.count;
    this.setState({ count });
  }

  render() {
    const count = this.state.count;
    return (
      <div>
        {ClientData.map((clientDetail, index) =>  {
        return (( index < count ) ? <ClientDetail client={clientDetail} key={`client-list-key ${index}`}/> : ''
        )})}
        <div style={styles}>
          <button style={showMoreButton} onClick={this.handleCount}> Show More
          </button>
        </div>
      </div>
    )
  };
}

const showMoreButton = {
  fontSize: 20,
  backgroundColor: '#4885FF',
  color: 'white',
  fontWeight: '500',
  padding: 10,
  marginTop: 10,
  border: '3px #4885FF solid',
  borderRadius: 4,
  boxShadow: '0 5px 13px 0 rgba(150, 183, 199, 0.24)',
  cursor: 'pointer',
  transition: "all ease .5s"
}



