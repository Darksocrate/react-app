import React , { Component } from 'react';
import { styles } from './ClientDetail';
import ClientDetails from './ClientDetails';


export default class ClientDetailsController extends Component {

  render() {
    const client = this.props;
    return (
      <div>
        {ClientData.map((clientDetail, index) =>  {
        return ( <ClientDetails client={clientDetail} key={`client-list-key ${index}`}/>
        )})}
      </div>
    )
  }
}