import React , { Component } from 'react';
import ClientData from '../data.json';
import ClientDetails from './ClientDetails';


export default class ClientDetail extends Component {
  
  constructor(props) {
    super(props);
    this.showDetails = this.showDetails.bind(this);
  }

  showDetails() {
    ClientData.map((clientDetail, index) =>  {
      return <ClientDetails client={clientDetail} key={`client-details-key ${index}`}/> 
      })
  }
  
  render() {
    const {client} = this.props;
    return (
     <div style={styles}>
      <li>{client.client}</li>
      <li style={town}>{client.ville.toLowerCase()}</li>
      <div>
        <div>
          <button style={buttonStyle} onClick={this.showDetails}>Show Details</button>
        </div>
      </div>
    </div>
    )
  }
}

export const styles = {
  marginTop: 20,
  padding: 20,
  fontSize: 30,
  textAlign: 'center',
  listStyleType: 'none',
  color: 'darkblue'
} 

const buttonStyle = {
  fontSize: 20,
  backgroundColor: '#1759FF',
  color: 'white',
  fontWeight: '500',
  padding: 10,
  marginTop: 10,
  border: '3px #1759FF solid',
  borderRadius: 4,
  boxShadow: '0 5px 13px 0 rgba(150, 183, 199, 0.24)',
  cursor: 'pointer',
  transition: "all ease .5s",
  ':hover' :{
    backgroundColor: '#EB3D3D'
  }
} 

const town = {
  color: '#335A92', 
  textTransform: 'capitalize'
}




