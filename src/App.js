import React from 'react';
import ClientList from './components/ClientList'
import './App.css';

function App() {
  return (
    <div className="App">
     <ClientList />
    </div>
  );
}

export default App;
