import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import ClientList from './components/ClientList';
// import ClientMap from './components/ClientMap';
// import ClientDetails from './components/ClientDetails';

ReactDOM.render(<App />, document.getElementById('root'));
ReactDOM.render(<ClientList />, document.getElementById('root'));
// ReactDOM.render(<ClientDetails />, document.getElementById('root'));
// ReactDOM.render(<ClientMap />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
